

Pod::Spec.new do |s|

s.name         = "XLHUD"

s.version      = "0.0.4"

s.summary      = "提示框项目初始化配置zzzzzzzzzzzzzzzzz"

s.description  = <<-DESC
提示框配置基础库zzzzzzzzzzzzzzzzzzzzzzzzzzz
DESC

s.homepage     = "https://gitee.com/xuelei163/XLHUDProgressView"

s.license      = { :type => "MIT", :file => 'LICENSE' }


s.author       = { "XLEE" => "18709264359@163.com" }

s.platform     = :ios, "9.0"

s.source       = { :git => "https://gitee.com/xuelei163/XLHUDProgressView.git", :tag => s.version }

s.source_files  ='XLHUDConfigureView/*.{h,m}'

#s.public_header_files = 'XLHUD/XLHUDView.h'

s.resources   = "XLHUDConfigureView/*.bundle"

s.frameworks = "UIKit"

s.requires_arc = true

s.dependency "Masonry"


end

