//
//  XLHUDView.h
//  XLHUD
//
//  Created by 薛磊 on 2018/6/12.
//  Copyright © 2018年 XL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,ModeType){
    /** 提示成功图片加文字*/
    ModeTypeSuccess,
    /** 提示失败图片加文字*/
    ModeTypeError,
    /** 提示文字无图片*/
    ModelTypeHide,
    /** 提示加载等待*/
    ModelTypeShowPregress,
    /** 取消加载等待*/
    ModelTypeHideProgress,
};


@interface XLProgressContentView : UIView
/** 图片*/
@property(nonatomic,strong)UIImageView * iconImageView;
/** 文字*/
@property(nonatomic,strong)UILabel * mainLabel;
/** 初始化自定义视图*/
-(instancetype)initWithTitle:(NSString *)title type:(ModeType)type;

@end


@interface XLHUDView : UIView
/** 显示的自定义视图*/
@property(nonatomic,strong)XLProgressContentView * contentView;


/** 提示成功图片加文字*/
+(instancetype)showSuccessText:(NSString *)text;
/** 提示失败图片加文字*/
+(instancetype)showErrorText:(NSString *)text;
/** 提示文字无图片*/
+(instancetype)showHideText:(NSString *)text;
/** 提示加载等待*/
+(instancetype)showPregress;
/** 取消加载等待*/
+(void)hidePregress;

@end



