//
//  XLHUDView.m
//  XLHUD
//
//  Created by 薛磊 on 2018/6/12.
//  Copyright © 2018年 XL. All rights reserved.
//

#import "XLHUDView.h"
#import <Masonry/Masonry.h>

static CGFloat const VerticalClearance = 10;

@interface XLProgressContentView ()


@end


@implementation XLProgressContentView

-(instancetype)initWithTitle:(NSString *)title type:(ModeType)type{
    
    self = [super initWithFrame:CGRectZero];
    if (self) {
        
        self.backgroundColor = [UIColor darkGrayColor];
        
        self.layer.cornerRadius = 6;
        
        self.layer.masksToBounds = true;
        
        self.userInteractionEnabled = false;
        
        _mainLabel = [UILabel new];
        _mainLabel.textColor = [UIColor whiteColor];
        _mainLabel.font = [UIFont systemFontOfSize:15];
        _mainLabel.numberOfLines = 0;
        _mainLabel.text = title;
        [self addSubview:_mainLabel];
    
        _iconImageView = [UIImageView new];
        [self addSubview:_iconImageView];
        
        [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.left.mas_equalTo(14);
            make.width.height.mas_equalTo(26);
        }];
        
        
        [_mainLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(VerticalClearance);
            make.right.mas_equalTo(-14);
            make.left.mas_equalTo(self.iconImageView.mas_right).offset(5);
            make.bottom.mas_equalTo(-VerticalClearance);
        }];
        
        switch (type) {
            case ModeTypeSuccess:
            {
                _iconImageView.image = [self imageNamedFromJPHUDBundle:@"hud_success"];
               
            }
                break;
            case ModeTypeError:
            {
                _iconImageView.image = [self imageNamedFromJPHUDBundle:@"hud_error"];
            }
                break;
            case ModelTypeHideProgress:
            {
                
            }
                break;
            case ModelTypeShowPregress:
            {
                _iconImageView.image = [self imageNamedFromJPHUDBundle:@"hud_progress"];
                CABasicAnimation *caAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
                caAnimation.toValue  = @(2 * M_PI);
                caAnimation.duration = 1.0;
                caAnimation.repeatCount = MAXFLOAT;
                [_iconImageView.layer addAnimation:caAnimation forKey:nil];
                _mainLabel.text = @"加载中";
                
            }
                break;
            case ModelTypeHide:
            {
                _iconImageView.image = [UIImage new];
                _iconImageView.hidden = true;
                [_iconImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.centerY.mas_equalTo(0);
                    make.left.mas_equalTo(5);
                    make.width.height.mas_equalTo(0);
                }];
                
                _mainLabel.textAlignment = NSTextAlignmentCenter;
                [_mainLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    make.edges.mas_equalTo(UIEdgeInsetsMake(VerticalClearance, 15, VerticalClearance, 15));
                    make.height.mas_greaterThanOrEqualTo(25);
                    make.width.mas_greaterThanOrEqualTo(100);
                }];
            }
                break;
            default:
                break;
        }
        
        
        
    }
    return self;
}

-(UIImage *)imageNamedFromJPHUDBundle:(NSString*)name{
    
    NSBundle *bundle = [NSBundle bundleForClass:[XLHUDView class]];
    NSURL *url = [bundle URLForResource:@"XLHUD" withExtension:@"bundle"];
    bundle = [NSBundle bundleWithURL:url];
    
    name = [name stringByAppendingString:@"@2x"];
    NSString *imagePath = [bundle pathForResource:name ofType:@"png"];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if (!image) {
        // 兼容业务方自己设置图片的方式
        name = [name stringByReplacingOccurrencesOfString:@"@2x" withString:@""];
        image = [UIImage imageNamed:name];
    }
    
    return image?:[UIImage new];
    
}

@end

@interface XLHUDView()

@end


@implementation XLHUDView

+(instancetype)showSuccessText:(NSString *)text view:(UIView *)view {

    return  [XLHUDView sharedHUDView:view title:text showType:ModeTypeSuccess];

}
+(instancetype)showErrorText:(NSString *)text view:(UIView *)view {
    
    return  [XLHUDView sharedHUDView:view title:text showType:ModeTypeError];
    
}
+(instancetype)showHideText:(NSString *)text view:(UIView *)view {
    
    return  [XLHUDView sharedHUDView:view title:text showType:ModelTypeHide];
    
}
+(instancetype)showWiteText:(NSString *)text view:(UIView *)view {
    
    return  [XLHUDView sharedHUDView:view title:text showType:ModelTypeShowPregress];
    
}
+(instancetype)hidePregressText:(NSString *)text view:(UIView *)view {
    
    return  [XLHUDView sharedHUDView:view title:text showType:ModelTypeHideProgress];
    
}
+ (instancetype)sharedHUDView:(UIView*)view title:(NSString*)title showType:(ModeType)showType {
    
        if (view == nil) {
            view = [UIApplication sharedApplication].keyWindow;
        }
    
        XLHUDView * vc = [[XLHUDView alloc] initWithText:title view:view type:showType];
    
        [view addSubview:vc];
    
        [view bringSubviewToFront:vc];
    
        return vc;
    
}

-(instancetype)initWithText:(NSString * )text view:(UIView *)view type:(ModeType)type{
    
    if (self = [super initWithFrame:view.frame]) {
        
        self.contentView = [[XLProgressContentView alloc] initWithTitle:text type:type];
        [self addSubview:self.contentView];
        
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.mas_equalTo(0);
            make.width.mas_lessThanOrEqualTo(view.frame.size.width * 0.7);
        }];
        
        
        
        if (type == ModelTypeShowPregress) {
   
        }
        else{
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [XLHUDView hidHudInView:view animated:YES];
            });
        }
        
        
        
    }
    
    return self;
}
+(void)hidHudInView:(UIView *)view animated:(BOOL)animated{
    
    for (XLHUDView * subview in view.subviews) {
        if ([subview isMemberOfClass:[XLHUDView class]]) {
            if (animated) {
                
                [subview removeFromSuperview];
            }
            else{
                [subview removeFromSuperview];
            }
        }
    }
    
}
+(instancetype)showSuccessText:(NSString *)text{
    
    return [XLHUDView showSuccessText:text view:nil];
    
}
+(instancetype)showErrorText:(NSString *)text{
    return [XLHUDView showErrorText:text view:nil];
}
+(instancetype)showHideText:(NSString *)text{
    return [XLHUDView showHideText:text view:nil];
}
+(instancetype)showPregress{
    
    return [XLHUDView showWiteText:nil view:nil];
}
+(void)hidePregress{
    
    [XLHUDView hidHudAnimated:YES];
}
+(void)hidHudAnimated:(BOOL)animated{
    
    [XLHUDView hidHudInView:[UIApplication sharedApplication].keyWindow animated:animated];
}
@end
